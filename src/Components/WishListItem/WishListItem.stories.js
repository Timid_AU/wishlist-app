import React from 'react';
import {storiesOf} from '@storybook/react';

import WishListItem from './WishListItem';

storiesOf("ListItem", module)
    .add('Item', () => {
        return (
            <WishListItem 
                name="Computer" 
                imageURL="https://www.jbhifi.com.au/FileLibrary/ProductResources/Images/284469-L-LO.jpg"
                price="2000.00"
                locationURL="https://www.google.com"
            />        
        )
    });