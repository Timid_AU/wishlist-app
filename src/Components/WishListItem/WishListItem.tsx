import React from 'react';
import styled from 'styled-components/macro';

const Wrapper = styled.div`
    margin: 10px;
    padding: 10px;
    height: 150px;
    width: 400px;
    box-sizing: border-box;
    border-radius: 5px;
    box-shadow:  2px 2px 5px 2px rgba(184,184,184,1);
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    &:hover {
        cursor: pointer;
    }
`;

const Image = styled('img')`
    height: 125px;
    width: 125px;
    alt: "";
    flex-grow: 1;
`;

const ItemName = styled.p`
    font-size: 25px;
    padding: 10px;
    flex-grow: 2;
`;

const PriceAndLocationDiv = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
    padding: 0px 5px;
    text-align: right;
    flex-grow: 1;
    text-transform: capitalize;
`;

const Price = styled.span`
    font-weight: bold;
`;

interface WishListItemProps {
    name: string;
    imageURL: string;
    price: string;
    locationURL: string;
}

const WishListItem: React.FunctionComponent<WishListItemProps> = (props) => {
    const shopName = props.locationURL.split('.')[1];
    return (
        <Wrapper>
            <Image src={props.imageURL}/>
            <ItemName>{props.name}</ItemName>
            <PriceAndLocationDiv>
                <a href={props.locationURL}>{shopName}</a>
                <Price>${props.price}</Price>
            </PriceAndLocationDiv>
        </Wrapper>
    )
}

export default WishListItem;