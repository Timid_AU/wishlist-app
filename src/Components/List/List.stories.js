import React from 'react';
import { storiesOf } from '@storybook/react';
import List from './List';

let list = [];
for(let i = 0; i < 25; i++)
{
    list.push(<p>Test list item {i}</p>)
}

storiesOf('List', module)
  .add('With Elements', () => (
        <List list={list}/>
    ))
  .add("Without Elements", () => (
      <List list={[]} />
    ))