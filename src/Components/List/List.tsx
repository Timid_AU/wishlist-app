import React from 'react';

interface ListProps {
    list: JSX.Element[];
}

const List: React.FunctionComponent<ListProps> = (props) => {
    return (
        <React.Fragment>
            {props.list.length ? props.list : <p>No items to display</p>}
        </React.Fragment>
    )
}

export default List;